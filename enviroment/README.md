# Przygotowanie Środowiska
Cześć,
Pierwszą i najważniejszą informacją jest fakt, że na warsztatach będziecie potrzebować własnego sprzętu.

W tej sekcji dowiesz się jak poprawnie skonfigurować środowisko developerskie, na którym będziemy pracowali podczas warsztatów.
## Oprogramowanie
Przyda się na pewno:
- IDE jak Visual Studio Code / Visual Studio / Rider. Szkoleniowiec podczas ćwiczeń będzie używać Rider-a.
- [Postman](https://www.getpostman.com/)
- Przeglądarka internetowa (Chrome / Firefox)
## Postman
Podczas warsztatów bedą wykonywane szyfrowane połaczenia lokalne (https i wss). Koniecznym jest skonfigurowanie Postman-a, aby nie walidował certyfikatów SSL. Wystarczy wejsć w ustawienia Postman-a i odznacz zaznaczoną na poniższym obrazku opcję.
![An image](https://linecode.pl/workshops/assets/postman.png)
## .NET Core
Podstawą do uczestnictwa w warsztatach jest posiadanie zainstalowanej platformy .NET Core w wersji 2.2+ na swoim komputerze. Potrzebne informacje SDK możecie znaleźć tutaj:
[https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download)
### Testowy Projekt
Po zainstalowaniu .NET Core SDK uruchom kilka komend w konsoli, aby sprawdzić czy wszystko działa poprawnie.
 
Zacznijmy od sprawdzenia dotnet CLI, polecenie:
```
dotnet –-version
```
Powinno wypisać na konsoli obecnie zainstalowaną wersję dotnet-a np. (2.2.101)
Następnie sprawdź, czy jesteś w stanie wygenerować nowy projekt wraz z solucją. W przypadku używania konsoli git bash lub systemu linux / mac os to gotowy skrypt masz poniżej. 
``` 
cd ./workspace
mkdir testProject
cd testProject
dotnet new sln
mkdir src
dotnet new mvc -n TestProject -o ./src/TestProject
dotnet sln add **/*csproj
dotnet restore
dotnet build
cd ./src/TestProject
dotnet run
```
Jeżeli jesteś użytkownikiem Windows-a polecam wygenerowanie projektu i uruchomienie go za pomocą Visual Studio.
 
### Instalcja certyfikatów SSL
Instalacja wymaga posiadania certyfikatów SSL na swoim urządzeniu.  Wykorzystaj dotnet CLI.
```
dotnet dev-certs https --trust
```
Po wykonaniu tej komendy zostaniesz poproszony o potwierdzenie, czy na pewno chcesz dodać certyfikat. Ponieważ systemy operacyjne nie będą mogły zweryfikować poprawności certyfikatu dla domeny „localhost”, po zaakceptowaniu komunikatu i zrestartowaniu aplikacji, powinna się ona odpalić już pod bezpiecznym połączeniem po protokole https.

![An Image](https://linecode.pl/workshops/assets/ssl.png)
## RabbitMQ
W późniejszej części warsztatów będziemy używali RabbitMQ aby rozproszyć komunikację pomiędzy kilkoma aplikacji. Serwer RabbitMQ możemy uruchomić na kilka sposobów, w zależności od systemu operacyjnego. Preferowane będzie wykorzystanie obrazu Docker-a.
### Windows
Aby zainstalować serwer RabbitMQ wraz z panelem administracyjnym musisz podążać za tutorialem wstawionym poniżej:
 
<iframe width="560" height="315" src="https://www.youtube.com/embed/3sEPqKrFQf8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
### Ubuntu
Aby zainstalować serwer RabbitMQ na Ubuntu musisz podążać zgodnie z tutorialem przedstawionym w oficjalnej dokumentacji RabbitMQ.
[Install RabbitMQ on Ubuntu or Debain](https://www.rabbitmq.com/install-debian.html)
### Mac OS
Aby zainstalować RabbitMQ na Mac OS możesz użyć narzędzia Homebrew. Wszystko jest opisane w oficjalnej dokumentacji RabbitMQ
[Install RabbitMQ on Mac OS by Homebrew](https://www.rabbitmq.com/install-homebrew.html)
### Docker
Preferowanym sposobem instalacji RabbitMQ jest wykorzystanie oficjalnego obrazu Docker-a. Aby zainstalować samego Docker-a trzeba przejść do oficjalnej dokumentacji: https://docs.docker.com/install/
Następnie możemy wywołać polecenie, które uruchomi serwer RabbitMQ w kontenerze.
```
docker run -p 15672:15672 -p 5672:5672 -d rabbitmq:3.7-management-alpine
```
Bardzo proszę o pobranie obrazu RabbitMQ przed warsztatami!
### Panel Zarządzania RabbitMQ
Po poprawnym zainstalowaniu i uruchomieniu serwera RabbitMQ powinieneś pod adresem: [http://localhost:15672](http://localhost:15672) zobaczyć panel zarządzania. Standardowym loginem i hasłem jest guest/guest.
![An Image](https://linecode.pl/workshops/assets/rabbit.png)
## Baza Danych
Ta wersja warsztatów nie potrzebuje komunikacji z bazą danych.

