---
home: true
heroImage: https://linecode.pl/workshops/assets/pexels-photo-265076.jpeg
actionText: Przygotowanie Środowiska →
actionLink: /enviroment/
# features:
# - title: .NET Core
# - details: Minimal setup with markdown-centered project structure helps you focus on writing.
# - title: SignalR
# - details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
# - title: RabbitMQ
# - details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: Copyright © 2019-present Kamil Kiełbasa - Linecode
---

