module.exports = {
    title: 'Warsztaty .NET Core',
    description: 'Wersja podstawowa',
    themeConfig: {
        nav: [
            { text: 'Przygotowanie Środowiska', link: '/enviroment/' },
            { text: 'MVC', link: '/part-1/' },
            { text: 'Auth', link: '/part-2/' },
            { text: 'SignalR', link: '/part-3/' },
            { text: 'RabbitMQ', link: '/part-4/' }
        ],
        sidebar: "auto"
    }
}